#!/usr/bin/python

# script for running one experiment
#
import subprocess as sp
import sys
import socket
import re

removed_options = ['-r']

""" Remove every opt in rm_opts.
rm_opts are optional exported files that need a prefix to construct a full path
Ex., rm_opts can be '--neighpath' or '-v'

 input:
   all_opts and rm_opts are both list of strs
 output:
   remain_opts: remained opts
   rm_args    : the removed opts with args
"""


def remove_option(all_opts, rm_opts, path_prefix, opt_len=1):
    remain_opts = " ".join(all_opts)
    remain_opts = re.split(" ", remain_opts)
    rm_args = []
    for rm in rm_opts:
        rm_idx = remain_opts.index(rm)
        rm_args = rm_args + remain_opts[rm_idx:(rm_idx + opt_len + 1)]
        del remain_opts[rm_idx:(rm_idx + opt_len + 1)]
    return remain_opts, rm_args


if socket.gethostname() == 'v570':
    bin_file = '/home/chenwx/workspace/ubcsat/ubcsat'
    out_dir = "/home/chenwx/workspace/ubcsat/res/"
else:
    bin_file = "/s/chopin/m/proj/sched/chenwx/ubcsat/ubcsat"
    out_dir = "/s/chopin/m/proj/sched/chenwx/ubcsat/res/"

if sys.argv[1] != "-i":
    print "instance path must be specified first"
    exit(1)

inst_name_argv_index = sys.argv.index("-inst")
inst_name = sys.argv[inst_name_argv_index + 1]

cnf_file = sys.argv[2]
print "inst_name", inst_name
run_options = sys.argv[inst_name_argv_index + 2:]
run_options, rm_args = remove_option(run_options, removed_options, out_dir, opt_len=2)
out_file = out_dir + inst_name + "".join(run_options) + ".out"
err_file = out_dir + inst_name + "".join(run_options) + ".err"

with open(out_file, "w") as out, open(err_file, "w") as err:
    cmd_str = [bin_file, "-i", cnf_file] + run_options + rm_args
    print cmd_str
    try:
        sp.check_call(cmd_str, stdout=out, stderr=err)
    except sp.CalledProcessError, e:
        print "error while executing", e  # except OSError:
    except OSError, e:
        print "executable", bin_file, "not found.", e
