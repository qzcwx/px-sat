#!/usr/bin/python

# script for generating jobs
import os

# fixed settings
bin_file = "/s/chopin/m/proj/sched/chenwx/ubcsat/py/onerun.py"
cmd_file = "commands.lst"
out_dir = "/s/chopin/m/proj/sched/chenwx/ubcsat/res"  # never use a relative path here

# parameters
inst_set_path = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.indu.small.tw.n.pre.desk"
# inst_set_path = "/home/chenwx/workspace/inst/lists/2014.crafted.indu.small.tw.n.pre.laptop"
runs = 10
rseed = 1
max_flip = "max"
max_evals = "max"
max_time_sec = 1000
alg_range = ["adaptg2wsat"]

# ./ubcsat -i INST -alg ALG -timeout 100 -cutoff max -numevals max -sytime -r bestsol FILE
with open(cmd_file, 'w') as out, open(inst_set_path, 'r') as inst_set:
    inst_lines = inst_set.readlines()
    for line in inst_lines:
        for alg in alg_range:
            cnf_name, cnf_file = line.split()
            out_file = "{}/{}-{}-timeout{}.sol" \
                .format(out_dir, cnf_name, alg, max_time_sec)

            if not os.path.isfile(out_file):
                job = "{} -i {} -inst {} -alg {} -cutoff {} -timeout {} -numevals {} -systime -seed {} -runs {} -r bestsol {}" \
                    .format(bin_file, cnf_file, cnf_name, alg, max_flip, max_time_sec, max_evals, rseed, runs, out_file)
                print >> out, job
            else:
                print out_file, "exist"
